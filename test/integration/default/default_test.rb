# # encoding: utf-8

# Inspec test for recipe ecmc_dell::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe package("snmpd") do
  it { should be_installed }
end

describe apt('http://linux.dell.com/repo/community/openmanage/911/xenial') do
  it { should exist }
  it { should be_enabled }
end

describe package("srvadmin-idracadm8") do
  it { should be_installed }
end

describe file('/var/lib/snmp/snmpd.conf') do
  its('content') { should include 'opennms'}
end

describe service('snmpd') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe command('snmpget -v 3 -u opennms -X encpassphrase -l authNoPriv -a MD5 -A passphrase localhost iso.3.6.1.2.1.1.1.0') do
  its('exit_status') { should eq 0 }
end
