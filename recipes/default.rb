#
# Cookbook:: ecmc_dell
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

execute 'add_key' do
  command 'gpg -a --export 1285491434D8786F | apt-key add -'
  action :nothing
  notifies :add, 'apt_repository[dell_osma]'
end

execute 'fetch_gpg' do
  command 'gpg --keyserver pool.sks-keyservers.net --recv-key 1285491434D8786F'
  notifies :run, 'execute[add_key]', :immediately
  not_if 'apt-key list | grep "34D8786F'
end

apt_repository 'dell_osma' do
  uri 'http://linux.dell.com/repo/community/openmanage/911/xenial'
  components ['main']
end

package 'snmp'
package 'libsnmp-dev'
package 'snmpd'

template '/etc/snmp/snmpd.conf' do
  source 'snmpd.conf.erb'
  owner 'root'
  group 'root'
  mode '660'
  variables :data_bag => data_bag_item('snmpd', 'usm')
end

usm_items = data_bag_item('snmpd', 'usm')

service 'snmpd' do
  action :stop
end

execute 'setup_usm' do
  command "net-snmp-create-v3-user -a #{usm_items[:passphrase]} -x #{usm_items[:encpassphrase]} -X DES opennms"
end

service 'snmpd' do
  action :restart
end

package 'srvadmin-idracadm8'
